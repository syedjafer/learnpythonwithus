# The usual - Find given number is even or odd

# Requirement
To find out the given number is even or odd

# Concepts needed
conditional statement, variables, inputs and operators

# Points to note
- Get a number from user
- Print even if the number is even and print odd if the number is odd

# Example
num = 3
expected output  : Odd
