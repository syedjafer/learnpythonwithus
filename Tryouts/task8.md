# profit or loss calculator

# Requirement
Based on the invested amount and returns qualify the output as profit or loss

# Concepts needed
control statements, variables, operators and inputs

# Points to note
- Get two variables (one for investment and one for returns)
- Based on the investment and returns calculate we have profit or not.
- If profit print "profit" and the profit amount
- If loss print "loss" and the loss amount

# Example
- Investment = 1000
- Returns = 900

expected output:
    loss
    -100


