set_a = {"apple", "banana", "orange"}
set_b = {"lemon", "jackfruit", "strawberry", "apple"}

# intersection
# res = set_a.intersection(set_b)
# print(res)

res = set_b.intersection(set_a)
print(res)

# intersection_update
# print(set_a)
# set_a.intersection_update(set_b)
# print(set_a)

list_a = [1, 2, 3]
list_b = [2,3, 4]

print(set(list_a).intersection(set(list_b)))
