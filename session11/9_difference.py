set_a = {"apple", "banana", "orange"}
set_b = {"lemon", "jackfruit", "strawberry", "apple"}


# difference

# res = set_a.difference(set_b)
# print(res)
print(set_a.isdisjoint(set_b))
set_b.difference_update(set_a)

print(set_a.isdisjoint(set_b))

# difference_update
# print(set_a)
# set_a.difference_update(set_b)
# print(set_a)
