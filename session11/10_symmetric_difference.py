set_a = {"apple", "banana", "orange"}
set_b = {"lemon", "jackfruit", "strawberry", "apple"}


#symmetric_difference
# res = set_a.symmetric_difference(set_b)
# print(res)


# symmetric_difference_update
print(set_a)
set_a.symmetric_difference_update(set_b)
print(set_a)
